import React, { Component } from 'react';
import { Card, CardImg, CardImgOverlay, CardText, CardBody, CardTitle } from 'reactstrap';

class DishDetail extends Component {

	constructor(props) {
		super(props);

		this.state = {
		}
	}

	renderDish(dish) {
        if (dish != null)
            return(
                <Card>
                    <CardImg top src={dish.image} alt={dish.name} />
                    <CardBody>
                      <CardTitle>{dish.name}</CardTitle>
                      <CardText>{dish.description}</CardText>
                    </CardBody>
                </Card>
            );
        else
            return(
                <div></div>
            );
    }

    renderComments(dish) {

    	const comments = this.props.dish ? (this.props.dish.comments.map((comment) => {
            return (
              <ul key={comment.id}>
                <p>{comment.comment}</p>
                <p>--{comment.author}, {comment.date.substring(0,10)}</p>
              </ul>
            );
        })): 
        (<div></div>);

        if (dish != null)
            return(
        		<div className="ul">
	        		<h4>Comments</h4>
	        		<div className="row">
	        			{comments}
	        		</div>
        		</div>
            );
        else
            return(
                <div></div>
            );
    }

	render() {



		return(
			<div className="row">
	          <div className="col-12 col-md-5 m-1">
	            {this.renderDish(this.props.dish)}
	          </div>
	          <div className="col-12 col-md-5 m-1">
	            {this.renderComments(this.props.dish)}
	          </div>
	        </div>
	    )
	}
}

export default DishDetail;